package com.greatus.magdrones.services;

import java.util.ArrayList;
import java.util.List;

import com.greatus.magdrones.dto.Command;
import com.greatus.magdrones.lang.MagDroneTask;
import com.greatus.magdrones.model.Dispatcher;
import com.greatus.magdrones.model.MagDrone;
import com.greatus.magdrones.repository.TubeStationRepository;
import com.greatus.magdrones.repository.TubeStationRepositoryImpl;

public class DispatcherServiceImpl extends EnhancedThreadPoolService<Command, MagDrone, MagDroneTask> implements DispatcherService{

	private static final int DEFAULT_CAPACITY_PER_DRONE = 10;
	
	private TubeStationRepository tubeRepo;
	
	
	public DispatcherServiceImpl() {
		super(new Dispatcher(2));
		tubeRepo = new TubeStationRepositoryImpl();
	}

	@Override
	public void issueCommand(Command command) {
		submitTask(command);
	}

	@Override
	public List<MagDrone> generateThreads() {
		List<MagDrone> returnValue = new ArrayList<>();
		returnValue.add(new MagDrone("5937"));
		returnValue.add(new MagDrone("6043"));
		return returnValue;
	}

	@Override
	protected int getCapacityPerThread() {
		return DEFAULT_CAPACITY_PER_DRONE;
	}

	@Override
	public void submitTask(Command command) {
		loadTask(new MagDroneTask(command, tubeRepo));
	}

}
