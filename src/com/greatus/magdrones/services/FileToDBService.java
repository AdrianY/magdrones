package com.greatus.magdrones.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.greatus.magdrones.util.EnhancedCache;
import com.greatus.magdrones.util.WorkerThread;

abstract class FileToDBService<D, C extends EnhancedCache<D>> extends EnhancedThreadPoolService<File, WorkerThread<Runnable>, Runnable> {

	private C dbCache;
	
	public FileToDBService(){
		super();
		dbCache = generateDBCache();
	}
	
	@Override
	public void submitTask(final File fileName) {
		loadTask(new Runnable(){
			public void run(){
				
				CSVFormat csvFileFormat = CSVFormat.DEFAULT.withAllowMissingColumnNames();
				CSVParser csvFileParser = null; 
	        	FileReader fileReader = null;
				try {
					fileReader = new FileReader(fileName);
					
					csvFileParser = new CSVParser(fileReader, csvFileFormat);
		            
		            List<CSVRecord> csvRecords = csvFileParser.getRecords(); 
		            
		            List<D> records = new ArrayList<>();
		            for (CSVRecord record: csvRecords) {
		            	records.add(generateDomain(record));
		            }
					dbCache.addRecords(records);
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
		            try {
		                fileReader.close();
		                csvFileParser.close();
		            } catch (IOException e) {
		            	logger().log(Level.WARNING, "Error while closing fileReader/csvFileParser !!!", e);
		            }
		        }
			}
		});
	}

	@Override
	public List<WorkerThread<Runnable>> generateThreads() {
		List<WorkerThread<Runnable>> returnValue = new ArrayList<>();
		returnValue.add(new WorkerThread<Runnable>(){

			@Override
			protected Runnable finalizeTask(Runnable task) {
				return task;
			}
			
		});
		return returnValue;
	}
	
	protected abstract D generateDomain(CSVRecord csvRecord);
	
	protected abstract C generateDBCache();
	
	protected abstract Logger logger();

}
