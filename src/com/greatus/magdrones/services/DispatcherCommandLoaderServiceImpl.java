package com.greatus.magdrones.services;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.greatus.magdrones.dto.Command;
import com.greatus.magdrones.lang.Location;
import com.greatus.magdrones.util.DateFormatterUtil;
import com.greatus.magdrones.util.WorkerThread;

public class DispatcherCommandLoaderServiceImpl extends EnhancedThreadPoolService<File, WorkerThread<Runnable>, Runnable> implements DispatcherCommandLoaderService{

	private static final Logger LOG = Logger.getLogger(DispatcherCommandLoaderServiceImpl.class.getName());
	
	private DispatcherService dispatcher;
	
	private DateFormatterUtil dateFormatterUtilBean;
	
	public DispatcherCommandLoaderServiceImpl(final DispatcherService dispatcher){
		this.dispatcher = dispatcher;
		this.dateFormatterUtilBean = DateFormatterUtil.generateInstance();
	}
	
	@Override
	public void loadCommandListFile(File file) {
		submitTask(file);
	}

	@Override
	public void submitTask(final File file) {
		loadTask(new Runnable(){
			public void run(){
				
				CSVFormat csvFileFormat = CSVFormat.DEFAULT.withAllowMissingColumnNames();
				CSVParser csvFileParser = null; 
	        	FileReader fileReader = null;
				try {
					fileReader = new FileReader(file);
					
					csvFileParser = new CSVParser(fileReader, csvFileFormat);
		            
		            List<CSVRecord> csvRecords = csvFileParser.getRecords(); 
		            
		            for (CSVRecord record: csvRecords) {
		            	dispatcher.issueCommand(parseCommand(record));
		            }
				} catch (IOException e) {
					e.printStackTrace();
				}finally {
		            try {
		                fileReader.close();
		                csvFileParser.close();
		            } catch (IOException e) {
		            	LOG.log(Level.WARNING, "Error while closing fileReader/csvFileParser !!!", e);
		            }
		        }
			}

			private Command parseCommand(CSVRecord record) {
				return new Command(
					record.get(0),
					new Location(Double.valueOf(record.get(1)), Double.valueOf(record.get(2))),
					dateFormatterUtilBean.parseLocalDateTime(record.get(3))
				);
			}
		});
	}

	@Override
	public List<WorkerThread<Runnable>> generateThreads() {
		List<WorkerThread<Runnable>> returnValue = new ArrayList<>();
		returnValue.add(WorkerThread.generateDefaultInstance());
		returnValue.add(WorkerThread.generateDefaultInstance());
		returnValue.add(WorkerThread.generateDefaultInstance());
		return returnValue;
	}
	
	@Override
	protected int getCapacityPerThread() {
		return 1000;
	}

}
