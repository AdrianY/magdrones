package com.greatus.magdrones.services;

import java.util.ArrayList;
import java.util.List;

import com.greatus.magdrones.util.BlockingQueue;
import com.greatus.magdrones.util.WorkerThread;

abstract class EnhancedThreadPoolService<V, W extends WorkerThread<T>, T extends Runnable> {

	private List<W> workerThreads;
	private BlockingQueue<T> taskQueue;
	
	EnhancedThreadPoolService(final BlockingQueue<T> taskQueue){	
		workerThreads = new ArrayList<>();
		workerThreads.addAll(generateThreads());
		this.taskQueue = taskQueue;
		startThreads();
	}
	
	EnhancedThreadPoolService(){	
		workerThreads = new ArrayList<>();
		workerThreads.addAll(generateThreads());
		int numberOfThreads = workerThreads.size();
		this.taskQueue = new BlockingQueue<>(numberOfThreads * getCapacityPerThread());
		startThreads();
	}
	
	public abstract void submitTask(V object);
	
	public abstract List<W> generateThreads();
	
	protected void loadTask(T task){
		try {
			taskQueue.put(task);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	protected abstract int getCapacityPerThread();
	
	public synchronized void shutDown(){
		for(WorkerThread<T> thread: workerThreads){
			thread.doStop();
		}
	}
	
	private void startThreads(){
		for(WorkerThread<T> thread: workerThreads){
			thread.setTaskQueue(taskQueue);
		}
		
		for(WorkerThread<T> thread: workerThreads){
			thread.start();
		}
	}
}
