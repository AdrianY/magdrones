package com.greatus.magdrones.services;

import com.greatus.magdrones.dto.Command;

public interface DispatcherService {
	void issueCommand(Command command);
}
