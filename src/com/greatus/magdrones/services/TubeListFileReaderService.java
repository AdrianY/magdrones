package com.greatus.magdrones.services;

import java.util.logging.Logger;

import org.apache.commons.csv.CSVRecord;

import com.greatus.magdrones.db.TubeCache;
import com.greatus.magdrones.lang.Location;
import com.greatus.magdrones.model.Tube;

public class TubeListFileReaderService extends FileToDBService<Tube, TubeCache> {

	private static final Logger LOG = Logger.getLogger(TubeListFileReaderService.class.getName());
	
	@Override
	protected Tube generateDomain(CSVRecord csvRecord) {
		return new Tube(
				csvRecord.get(0), 
				new Location(Double.parseDouble(csvRecord.get(1)), Double.parseDouble(csvRecord.get(2))));
	}

	@Override
	protected TubeCache generateDBCache() {
		return TubeCache.getInstance();
	}

	@Override
	protected int getCapacityPerThread() {
		return 1000;
	}

	@Override
	protected Logger logger() {
		return LOG;
	}

}
