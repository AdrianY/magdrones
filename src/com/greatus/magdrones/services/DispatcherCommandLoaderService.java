package com.greatus.magdrones.services;

import java.io.File;

public interface DispatcherCommandLoaderService {
	void loadCommandListFile(final File file);
}
