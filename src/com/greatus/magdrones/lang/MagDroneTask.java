package com.greatus.magdrones.lang;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import com.greatus.magdrones.dto.Command;
import com.greatus.magdrones.model.MagDrone;
import com.greatus.magdrones.model.Tube;
import com.greatus.magdrones.repository.TubeStationRepository;
import com.greatus.magdrones.util.DateFormatterUtil;

public class MagDroneTask implements Runnable{
	
	private boolean validTask;
	
	private Command commandDetails;
	
	private static final String[] trafficSituationTypes = {"HEAVY","LIGHT","MODERATE"};
	
	private TubeStationRepository tubeRepo;
	
	private DateFormatterUtil dateFormatterUtilBean;
	
	public MagDroneTask(final Command command, final TubeStationRepository repository){
		this.commandDetails = command;
		this.tubeRepo = repository;
		this.dateFormatterUtilBean = DateFormatterUtil.generateInstance();
	}
	
	public String getForDroneId() {
		return commandDetails.droneId;
	}

	public void setValidTask(boolean validTask) {
		this.validTask = validTask;
	}
	
	public Command getCommandDetails() {
		return commandDetails;
	}

	@Override
	public final void run() {
		if(validTask){
			doActualTask();
		}
	}
	
	public void doActualTask(){
		List<Tube> tubesObservableInRange = 
				tubeRepo.findStationsWithinRadius(commandDetails.getLocation(), MagDrone.MAG_DRONE_V1_RANGE);
		
		if(null != tubesObservableInRange && !tubesObservableInRange.isEmpty()){
			for(Tube tube: tubesObservableInRange){
				logReportToConsole(
					new TrafficReport(
							commandDetails.droneId,
							tube.stationName,
							getTrafficSituation(),
							LocalDateTime.now()
					)
				);
			}
		}
	}
	
	private String getTrafficSituation(){
		Random rand = new Random();
		return trafficSituationTypes[rand.nextInt(3)];
	}
	
	private void logReportToConsole(final TrafficReport report){
		String format = "%-20s %4s\n%-20s %4s\n%-20s %4s\n%-20s %4s\n\n";
		System.out.format(format, 
				"Drone Id:", report.droneId, 
				"Station:",report.station, 
				"Traffic situation:",report.trafficSituation, 
				"Time:",dateFormatterUtilBean.formatLocalDateTime(report.getTimeStamp())
		);
	}
	
	private static class TrafficReport{
		final String droneId;
		
		final String station;
		
		final String trafficSituation;
		
		private LocalDateTime timeStamp;

		public TrafficReport(String droneId, String station,
				String trafficSituation, LocalDateTime timeStamp) {
			this.droneId = droneId;
			this.station = station;
			this.trafficSituation = trafficSituation;
			this.timeStamp = timeStamp;
		}

		public LocalDateTime getTimeStamp() {
			return timeStamp;
		}
	}
}
