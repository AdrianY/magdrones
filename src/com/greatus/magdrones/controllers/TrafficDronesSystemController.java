package com.greatus.magdrones.controllers;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import com.greatus.magdrones.services.DispatcherCommandLoaderService;
import com.greatus.magdrones.services.DispatcherCommandLoaderServiceImpl;
import com.greatus.magdrones.services.DispatcherService;
import com.greatus.magdrones.services.DispatcherServiceImpl;
import com.greatus.magdrones.services.TubeListFileReaderService;

public class TrafficDronesSystemController {
	
	private TubeListFileReaderService tubeListService;
	
	private DispatcherService dispatcherService;
	
	private DispatcherCommandLoaderService commandLoaderService;
	
	private Logger logger;
	
	public TrafficDronesSystemController(){
		tubeListService = new TubeListFileReaderService();
		dispatcherService = new DispatcherServiceImpl();
		commandLoaderService = new DispatcherCommandLoaderServiceImpl(dispatcherService);
		logger = generateLogger();
	}
	
	public void startSystemProcess(
			String tubeLocationList, 
			String droneOneCommandList, 
			String droneTwoCommandList
	){
		logger.log(Level.INFO, "Starting drone tube traffic scan");
		tubeListService.submitTask(new File(tubeLocationList));
		commandLoaderService.loadCommandListFile(new File(droneOneCommandList));
		commandLoaderService.loadCommandListFile(new File(droneTwoCommandList));
	}
	
	public void stopSystemProcess(){
		logger.log(Level.INFO, "Shutting down drone tube traffic scan");
	}
	
	public static Logger generateLogger(){
		Logger logger = Logger.getLogger(TrafficDronesSystemController.class.getName());
		logger.addHandler(new StreamHandler(System.out, new SimpleFormatter()));
		return logger;
	}
}
