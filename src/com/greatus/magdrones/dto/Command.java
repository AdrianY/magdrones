package com.greatus.magdrones.dto;

import java.time.LocalDateTime;

import com.greatus.magdrones.lang.Location;

public class Command {
	
	private Location location;
	
	public final String droneId;
	
	private LocalDateTime timeStamp;
	
	public Command(String droneId, final Location location, final LocalDateTime timeStamp){
		this.droneId = droneId;
		this.timeStamp = timeStamp;
		this.location = location;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public Location getLocation() {
		return location;
	}
}
