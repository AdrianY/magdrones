package com.greatus.magdrones.util;

public class MathUtil {
	
	public static MathUtil generateInstance(){
		return new MathUtil();
	}
	
	public double getDistance(double originX,double originY, double destX, double destY){
		return Math.sqrt(axisDistance(originX, originY) + axisDistance(destX, destY));
	}
	
	private double axisDistance(double originX,double originY){
		return Math.sqrt(Math.pow(originX - originY, 2));
	}
}
