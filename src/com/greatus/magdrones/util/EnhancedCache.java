package com.greatus.magdrones.util;

import java.util.ArrayList;
import java.util.List;

public abstract class EnhancedCache<V> {
	protected List<V> cache;
	
	public EnhancedCache(){
		cache = new ArrayList<>();
	}
	
	public abstract boolean exists(V record); 
	
	public List<V> getRecords(){
		return cache;
	}
	
	public void addRecords(final List<V> records){
		cache.addAll(records);
	}
	
	public int size(){
		return cache.size();
	}
}
