package com.greatus.magdrones.util;

import java.util.ArrayList;
import java.util.List;

public class BlockingQueue<V extends Runnable> {
	private List<V> taskItems;
	
	private int capacity;
	
	public BlockingQueue(int capacity){
		taskItems = new ArrayList<>();
		this.capacity = capacity;
	}
	
	public synchronized void put(V taskItem) throws InterruptedException{
		while(capacity <= taskItems.size()){
			wait();
		}
		
		if(capacity > taskItems.size())
			notifyAll();
		
		taskItems.add(taskItem);
	}

	public synchronized V releaseTaskItem() throws InterruptedException{
		while(0 == taskItems.size()){
			wait();
		}
		
		if(0 < taskItems.size())
			notifyAll();
		
		return taskItems.remove(0);
	}
}
