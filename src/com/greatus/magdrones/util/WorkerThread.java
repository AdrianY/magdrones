package com.greatus.magdrones.util;


public abstract class WorkerThread<V extends Runnable> extends Thread {
	protected boolean isStopped;
	protected BlockingQueue<V> taskQueue;
	
	public WorkerThread(){
		isStopped = false;
	}
	
	public void run(){
		while(!isStopped){
			try {
				V task = finalizeTask(taskQueue.releaseTaskItem());
				task.run();	
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	protected abstract V finalizeTask(V task) throws InterruptedException;
	
	public void setTaskQueue(final BlockingQueue<V> taskQueue){
		this.taskQueue = taskQueue;
	}
	
	public synchronized void doStop(){
		if(!isStopped) isStopped = true;
	}
	
	public static WorkerThread<Runnable> generateDefaultInstance(){
		return new WorkerThread<Runnable>(){

			@Override
			protected Runnable finalizeTask(Runnable task) {
				return task;
			}
			
		};
	}
}
