package com.greatus.magdrones.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatterUtil {
	
	private static final String LOCAL_DATE_TIME = "MM/dd/yyyy HH:mm";
	
	public static DateFormatterUtil generateInstance(){
		return new DateFormatterUtil();
	}
	
	private DateFormatterUtil(){}
	
	public LocalDateTime parseLocalDateTime(final String localDateTimeStr){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return LocalDateTime.parse(localDateTimeStr, formatter);
	}
	
	public String formatLocalDateTime(final LocalDateTime localDateTime){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LOCAL_DATE_TIME);
		return localDateTime.format(formatter);
	}
}
