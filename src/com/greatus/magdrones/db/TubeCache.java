package com.greatus.magdrones.db;

import com.greatus.magdrones.model.Tube;
import com.greatus.magdrones.util.EnhancedCache;

public class TubeCache extends EnhancedCache<Tube> {
	
	private static TubeCache instance = new TubeCache();
	
	public static TubeCache getInstance(){
		return instance;
	}

	private TubeCache() {
		super();
	}

	@Override
	public boolean exists(Tube record) {
		return false;
	}

}
