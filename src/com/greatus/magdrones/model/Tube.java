package com.greatus.magdrones.model;

import com.greatus.magdrones.lang.Location;

public class Tube{
	public final String stationName;
	
	private Location location;
	
	public Tube(String stationName, Location location) {
		this.stationName = stationName;
		this.location = location;
	}

	public Location getLocation() {
		return location;
	}
}
