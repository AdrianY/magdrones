package com.greatus.magdrones.model;

import com.greatus.magdrones.lang.MagDroneTask;
import com.greatus.magdrones.util.BlockingQueue;

public class Dispatcher extends BlockingQueue<MagDroneTask>{

	private static final int DEFAULT_CAPACITY_PER_DRONE = 10;
	
	private static final Long DISPATCHE_COMMAND_TX_SPEED = 250L;
	
	public Dispatcher(int numberOfDrones) {
		super(numberOfDrones * DEFAULT_CAPACITY_PER_DRONE);
	}

	public synchronized void put(MagDroneTask taskItem) throws InterruptedException{
		String format = "%-20s %4s\n%-20s %4s\n\n";
		System.out.format(format, 
				"Dispatcher to Drone Id:", taskItem.getCommandDetails().droneId.trim(), 
				"Go to location:",taskItem.getCommandDetails().getLocation().toString()
		);
		Thread.sleep(DISPATCHE_COMMAND_TX_SPEED);
		super.put(taskItem);
	}
}
