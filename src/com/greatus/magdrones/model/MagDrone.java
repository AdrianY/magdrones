package com.greatus.magdrones.model;

import com.greatus.magdrones.lang.MagDroneTask;
import com.greatus.magdrones.util.WorkerThread;

public class MagDrone extends WorkerThread<MagDroneTask> {
	
	public static final double MAG_DRONE_V1_RANGE = 350.0;
	
	private static final Long MAG_DRONE_V1_SPEED = 400L;

	public final String id;
	
	public MagDrone(final String id) {
		super();
		this.id = id;
	}

	@Override
	protected MagDroneTask finalizeTask(final MagDroneTask task) throws InterruptedException {
		boolean isThisDroneTask = id.equals(task.getForDroneId());
		task.setValidTask(isThisDroneTask);
		if(isThisDroneTask){
			Thread.sleep(MAG_DRONE_V1_SPEED);
		}
		return task;
	}
	
	
}
