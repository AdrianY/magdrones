package com.greatus.magdrones.views;

import java.util.Scanner;

import com.greatus.magdrones.controllers.TrafficDronesSystemController;

public class Runner {

	private TrafficDronesSystemController systemController;
	
	Runner(){
		systemController = new TrafficDronesSystemController();
	}
	
	private static final String DEFAULT_TUBE_LIST = "./tube.csv";
	
	private static final String DRONE_5937_COMMAND_LIST = "./5937.csv";
	
	private static final String DRONE_6043_COMMAND_LIST = "./6043.csv";
	
	public void executeSystemProcess(
		final String tubeList, 
		final String droneOneCommandList, 
		final String droneTwoCommandList
	){
		systemController.startSystemProcess(
				strIsEmpty(tubeList) ? DEFAULT_TUBE_LIST: tubeList, 
				strIsEmpty(droneOneCommandList) ? DRONE_5937_COMMAND_LIST: droneOneCommandList, 
				strIsEmpty(droneTwoCommandList) ? DRONE_6043_COMMAND_LIST: droneTwoCommandList
		);
	}
	
	private boolean strIsEmpty(final String str){
		return str == null || str.replaceAll("\"","").trim().isEmpty();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Runner runner = new Runner();
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter tube list CSV file location: ");
	    String tubeList = scanner.next();

	    System.out.print("Enter Drone 5937's command list csv file: ");
	    String droneOneCommandList = scanner.next();
	    
	    System.out.print("Enter Drone 6043's command list csv file: ");
	    String droneTwoCommandList = scanner.next();

	    System.out.print("Start system? ");
	    
	    String answer = scanner.next();
	    if("Y".equals(answer)){
	    	runner.executeSystemProcess(tubeList, droneOneCommandList, droneTwoCommandList);
	    }else{
	    	System.out.print("Shutting down system...");
	    }
	}

}
