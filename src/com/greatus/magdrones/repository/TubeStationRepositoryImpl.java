package com.greatus.magdrones.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.greatus.magdrones.db.TubeCache;
import com.greatus.magdrones.lang.Location;
import com.greatus.magdrones.model.Tube;
import com.greatus.magdrones.util.MathUtil;

public class TubeStationRepositoryImpl implements TubeStationRepository{
	
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(TubeStationRepositoryImpl.class.getName());
	
	private TubeCache cache;
	
	private MathUtil mathUtilBean;
	
	public TubeStationRepositoryImpl(){
		cache = TubeCache.getInstance();
		mathUtilBean = MathUtil.generateInstance();
	}

	@Override
	public List<Tube> findStationsWithinRadius(Location location, double radius) {
		List<Tube> returnValue = new ArrayList<>();
		
		for(Tube tube: cache.getRecords()){
			if(tubeWithinStatedDistance(tube, location, radius)){
				returnValue.add(tube);
			}
		}
		return returnValue;
	}

	private boolean tubeWithinStatedDistance(Tube tube, Location origin, double radius){
		Location tubeLocation = tube.getLocation();
		double distance = (mathUtilBean.getDistance(
				origin.latitude, tubeLocation.latitude, 
				origin.longitude, tubeLocation.longitude
		) * 1000);
		return radius >= distance;
	}
}
