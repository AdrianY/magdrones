package com.greatus.magdrones.repository;

import java.util.List;

import com.greatus.magdrones.lang.Location;
import com.greatus.magdrones.model.Tube;

public interface TubeStationRepository {
	List<Tube> findStationsWithinRadius(Location location, double radius);
}
